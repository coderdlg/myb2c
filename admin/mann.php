<?php
include '../include/common.inc.php';
$adminhelp=new Adminhelp();
if(!$q=$adminhelp->mcheckol()){
    header("Location:".$C['SITE_URL']."/admin/login.php");
    exit;
}
$adminhelp->mupdatesession();
if(!$action){
    $perpage=10;
    $pagevar="page";
    $q2=$db->query("select id from ann");
    
    $pages=new Fpage();
    $pages->totalNums=$db->num_rows($q2);
    $pages->perpageNum=$perpage;
    $pages->pageVar=$pagevar;
    $pages->jump_pageinputId="jumppage";
    $pagestr=$pages->showpages();
    $page=($$pagevar && is_numeric($$pagevar))?$$pagevar:0;
    $leftstart=$perpage*(max(0,$page-1));
    $q3=$db->query("select * from ann order by id desc limit {$leftstart},{$perpage} ");
    $annlist=array();
    while($row=$db->fetch_array($q3)){       
        $annlist[]=$row;
    }
    unset($q3);
    $tpl->assign("annlist",$annlist);
    $tpl->assign("page",$pagestr);
    $tpl->display("admin/mann.html");
}elseif($action == "add"){
    if($help->submitcheck()){
        if(!$title){
            exit("请输入标题");
        }
        if(!$content){
            exit("请输入内容");
        }
        if(!is_numeric($time)){
            exit("或时间不合法");
        }
        
        $ptime=$time==0?0:time()+86400*$time;
        $db->query("insert into ann (title,content,time) values ('{$title}','{$content}',{$ptime})");
        $help->showmessage("添加成功",-1,3);
    }else{
        $tpl->display("admin/mann_add.html");
    }
    
    
    
    
    
}elseif($action=="edit"){
    if(!$annid){
        exit("未指定ID");
    }
    if($submit){
        if(!$title){
            exit("未输入标题");
        }
        if(!is_numeric($time)){
            exit("未指定有效时间");
        }
        $ptime=$time==0?0:time()+86400*$time;
        $db->query("update ann set title='{$title}',content='{$content}',time={$ptime} where id={$annid}");
        $help->showmessage("更新成功",-1,3);
    }else{
        $q=$db->fetch_first("select * from ann where id={$annid}");
        if(!$q){
            exit("公告不存在");
        }
        $tpl->assign("anninfo",$q);
        $tpl->display("admin/mann_edit.html");
    }
}elseif($action=="delete"){
    if(!$annid){
        exit("未指定ID");
    }
    $db->query("delete from ann where id = {$annid}");
    $help->showmessage("删除成功",-1,3);
            
}
