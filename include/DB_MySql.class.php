<?php
/* 数据库操作类，仅支持MySql
 * 
 * 
 * 
 */
class DB_MySql {
    //put your code here
    private $link=null;
    function __construct() {                        
    }
    function connect($server="localhost",$username="root",$password="",$dbname=""){
        $this->link =  mysql_connect($server, $username, $password);
        if(!$this->link){
            die("数据库链接失败，请检查配置是否正确<br>".  mysql_errno().":".  mysql_error());
        }
        mysql_select_db($dbname , $this->link) or die("数据库{$dbname}不存在<br>". mysql_errno().":". mysql_error());
        mysql_set_charset('utf8',$this->link);                
    }
    function query($sql){
        
        
        $q =  mysql_query($sql,  $this->link);
        if($q){
            return $q;
        }else{
            die(mysql_errno().":". mysql_error());
        }       
    }
    
    function fetch_array($query,$result_type=MYSQL_ASSOC){
        return mysql_fetch_array($query, $result_type);
        
    }
    
    function fetch_first($sql){
        return $this->fetch_array($this->query($sql));
    }
    
    function affetched_rows(){
        return mysql_affected_rows($this->link);
    }
    
    function result($query,$row=0){
        $query=@mysql_result($query, $row);
        return $query;
    }
    
    function num_rows($query){
        return mysql_num_rows($query);
    }
    
    function num_fields($query){
        return mysql_num_fields($query);
    }
    
    function free_result($query){
        return mysql_free_result($query);
    }
    
    function insert_id(){
        return ($id = mysql_insert_id($this->link)) >= 0 ? $id : $this->result($this->query("SELECT last_insert_id()"), 0);
    }
    
    function fetch_row($query) {
	$query = mysql_fetch_row($query);
	return $query;
    }
    
    function fetch_fields($query) {
	return mysql_fetch_field($query);
    }
    
    
    function __destruct() {
        @mysql_close($this->link);
    }
}

?>
