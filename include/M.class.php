<?php
class M {
    
    /*
     * $lv int类型，-1表示读取所有分类，其余则读取对应LV值的分类
     * 支持到2级分类
     */
    function gettypeoptions(){
        
        global $db;
        $arr=array();
        $q=$db->query("select id,name,pid,path,concat(path,'-',id) as dpath from types order by dpath");
        while($row=$db->fetch_array($q)){
            $count=count(explode('-',$row['path']));
            $row['n']="|";
            for($i=0;$i<$count*4;$i++){
                $row['n'].='-';
            }
            $row['n'].='|';
            $arr[]=$row;
        }
        return $arr;
    }
    /*
     * 删除分类
     * 
     */
    function deltypes($typeid){
        global $db;
        if(is_array($typeid)){
            $str="('";
            $str.=implode("','", $typeid);
            $str.="')";
            $q=$db->query("select id from commo where typeid in {$str}");
            if($q){
                $commoid=array();
                while($row=$db->fetch_array($q)){
                    $commoid[]=$row['id'];
                }
                $this->delcommo($commoid);
            }
            $db->query("delete from types where id in {$str}");
            return true;
        }elseif(is_numeric($typeid)){
            $q=$db->query("select id from commo where typeid = {$typeid}");
            if($q){
                $commoid=array();
                while($row=$db->fetch_array($q)){
                    $commoid[]=$row['id'];
                }
                $this->delcommo($commoid);
            }
            $db->query("delete from types where id = {$typeid}");
            return true;
        }
    }
    /*
     * 删除商品
     */
    function delcommo($commoid){
        global $db,$C;
        if(is_array($commoid)){
            $str="('";
            $str.=implode("','", $commoid);
            $str.="')";
            $q2=$db->query("select * from attachment where commoid in {$str}");
            while($q1=$db->fetch_array($q2)){
                $path=ROOT.$C['attpath'].$q1['path'];
                if(file_exists($path)){
                    if(!unlink($path)){
                        exit("删除失败：删除附件时失败,aid为".$q1['aid']);
                    }
                }
            }
            /*
            * 删除数据库相关记录
            */
            $db->query("delete from commo where id in {$str}");
            $db->query("delete from attachment where commoid in {$str}");
            return true;
        }elseif(is_numeric($commoid)){
            $q=$db->fetch_first("select * from commo where id = {$commoid}");
            if(!$q){
                exit("商品不存在或已删除");
            }
            /*
            * 删除关联附件
            */
            $q2=$db->query("select * from attachment where commoid={$commoid}");
            while($q1=$db->fetch_array($q2)){
                $path=ROOT.$C['attpath'].$q1['path'];
                if(file_exists($path)){
                    if(!unlink($path)){
                        exit("删除失败：删除附件时失败,aid为".$q1['aid']);
                    }
                }
            }
            /*
            * 删除数据库相关记录
            */
            $db->query("delete from commo where id={$commoid}");
            $db->query("delete from attachment where commoid={$commoid}");
        return true;
        }else{
            return false;
        }
    }
    
    
}

?>
