<?php
/* 辅助函数类库，常用的一些辅助函数集成在这里面
 * 
 * 
 * 
 */
class Help {
    private $authkey="myb2c1";//用于加密解密的字符串，为了安全，定期修改
    function __construct() {
       
    }
    /*authcode加密解密函数
     * $string 需要加密或解密的字符串
     * $operation 默认解密，若加密则设置为ENCODE
     */
    function authcode($string, $operation = 'DECODE', $key = "", $expiry = 0) {

	$ckey_length = 4;
	$key = md5($key ? $key : $this->authkey);//若未指定key，则使用本类的私有成员authkey
	$keya = md5(substr($key, 0, 16));
	$keyb = md5(substr($key, 16, 16));
	$keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length): substr(md5(microtime()), -$ckey_length)) : '';

	$cryptkey = $keya.md5($keya.$keyc);
	$key_length = strlen($cryptkey);

	$string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0).substr(md5($string.$keyb), 0, 16).$string;
	$string_length = strlen($string);

	$result = '';
	$box = range(0, 255);

	$rndkey = array();
	for($i = 0; $i <= 255; $i++) {
		$rndkey[$i] = ord($cryptkey[$i % $key_length]);
	}

	for($j = $i = 0; $i < 256; $i++) {
		$j = ($j + $box[$i] + $rndkey[$i]) % 256;
		$tmp = $box[$i];
		$box[$i] = $box[$j];
		$box[$j] = $tmp;
	}

	for($a = $j = $i = 0; $i < $string_length; $i++) {
		$a = ($a + 1) % 256;
		$j = ($j + $box[$a]) % 256;
		$tmp = $box[$a];
		$box[$a] = $box[$j];
		$box[$j] = $tmp;
		$result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
	}

	if($operation == 'DECODE') {
		if((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26).$keyb), 0, 16)) {
			return substr($result, 26);
		} else {
			return '';
		}
	} else {
		return $keyc.str_replace('=', '', base64_encode($result));
	}

    }
    //参数全局安全过滤
    
    function safesql(){
        if(!get_magic_quotes_gpc()){
            foreach($_GET as $k => $v){
                $_GET[$k]=trim(htmlspecialchars(mysql_real_escape_string($v)));
            }
            foreach($_POST as $k => $v){
                $_POST[$k]=trim(htmlspecialchars(mysql_real_escape_string($v)));
            }
        }
        return 1;
    }
    //随机产生$num位字符串
    function mkrandomstr($num=6){
        $str="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $str=str_shuffle($str);
        return substr($str, 0,$num);
    }
    
    //获取字数
    function utf8_strlen($string = null) {
        // 将字符串分解为单元
        preg_match_all("/./us", $string, $match);
        // 返回单元个数
        return count($match[0]);        
    }
    //提示信息
    function showmessage($str,$re="",$time=5){
        global $tpl;
        $referer="";
        $err="";
        switch ($re){
            case -1:
                $referer=$_SERVER['HTTP_REFERER'];                
                break;
            default:  
                $referer=$re;
        }
        
        $tpl->assign("waittime",$time);
        $tpl->assign("referer",$referer);
        $tpl->assign("msg",$str);
        $tpl->display("message.html");
        
        exit;
    }
    /*
     * 表单提交校验
     * $name 提交按钮的name值
     */
    function submitcheck($name="submit"){
        if(isset($_REQUEST[$name])){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    /*
     * 正则验证
     * 返回的是错误信息
     */
    function checksth($str,$pattern){
        $arr=explode('|',$pattern);
        foreach ($arr as $v){
            switch ($v){
                case 'safe':
                    if(preg_match("/[^a-zA-Z0-9_]/", $str)){
                        return "含有非法字符";
                    }
                    break;
                case 'email':
                    if(preg_match("/^[\w\-\.]+@[\w\-\.]+(\.\w+)+$/", $str)){
                        return "email不合法";
                    }
                    break;
                default :
                    
                   
                        
            }
        }
        return 0;
    }
    
}
    
?>
